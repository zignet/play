using System;
using MonoTouch.UIKit;
using Example_Drawing.Screens.iPad.TouchDrawing;

namespace Example_Drawing.Screens.iPad.Eric
{
	public class Controller : UIViewController
	{
		#region -= constructors =-

		public Controller () : base() { }

		#endregion

		public override void LoadView ()
		{
			Console.WriteLine ("LoadView() Called");
			base.LoadView ();
			


//			View.BackgroundColor = UIColor.White;
//			var view = new DrawView (View.Frame, this);
		}

		public override void ViewDidLoad ()
		{
			base.ViewDidLoad ();


			var view = new DrawView (View.Frame, null);
			view.BackgroundColor = UIColor.White;

			this.View.Add (view);
			
		}
	}
}

