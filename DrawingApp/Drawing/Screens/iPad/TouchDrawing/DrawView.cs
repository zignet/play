using System;
using MonoTouch.UIKit;
using MonoTouch.CoreGraphics;
using System.Drawing;
using System.Collections.Generic;
using MonoTouch.Foundation;

namespace Example_Drawing
{
	public class DrawView : UIView
	{
		List<DrawPoint> Plot;
		public CGColor DrawingColor { get; set; }
		private PointF touchLocation;
		private PointF prevTouchLocation;
		private CGPath drawPath;
		private bool fingerDraw;
		
		DrawController dvc;
		
		// clear the canvas
		public void Clear ()
		{
			drawPath.Dispose ();
			drawPath = new CGPath ();
			fingerDraw = false;
			SetNeedsDisplay ();
			
		}

		// pass in a reference to the controller, although I never use it and could probably remove it
		public DrawView (RectangleF frame, DrawController root) : base(frame)
		{
			dvc = root;
			this.drawPath = new CGPath ();

			var image = UIImage.FromFile ("Images/PosturalDiagram.gif");
			if (image != null)
				this.BackgroundColor = UIColor.FromPatternImage(image);

			DrawingColor = UIColor.Red.CGColor;
			Plot = new List<DrawPoint> ();
		}

		public override void TouchesBegan (MonoTouch.Foundation.NSSet touches, UIEvent evt)
		{
			base.TouchesBegan (touches, evt);
			
			UITouch touch = touches.AnyObject as UITouch;
			this.fingerDraw = true;
			this.touchLocation = touch.LocationInView (this);
			this.prevTouchLocation = touch.PreviousLocationInView (this);
			this.SetNeedsDisplay ();
			
		}
		
		public override void TouchesMoved (MonoTouch.Foundation.NSSet touches, UIEvent evt)
		{
			base.TouchesMoved (touches, evt);
			
			UITouch touch = touches.AnyObject as UITouch;
			this.touchLocation = touch.LocationInView (this);
			this.prevTouchLocation = touch.PreviousLocationInView (this);
			this.SetNeedsDisplay ();



		}

		public UIImage GetDrawingImage ()
		{
			UIImage returnImg = null;
			
			UIGraphics.BeginImageContext (this.Bounds.Size);
			
			using (CGContext context = UIGraphics.GetCurrentContext()) {
				context.SetStrokeColor (DrawingColor);
				context.SetLineWidth (5f);
				context.SetLineJoin (CGLineJoin.Round);
				context.SetLineCap (CGLineCap.Round);
				context.AddPath (this.drawPath);
				context.DrawPath (CGPathDrawingMode.Stroke);
				returnImg = UIGraphics.GetImageFromCurrentImageContext ();
			}
			
			UIGraphics.EndImageContext ();


			return returnImg;
		}
		public static UIImage MakeCalendarBadge (UIImage template, string smallText, string bigText) 
		{
			using (var cs = CGColorSpace.CreateDeviceRGB ()){
				using (var context = new CGBitmapContext (IntPtr.Zero, 57, 57, 8, 57*4, cs, CGImageAlphaInfo.PremultipliedLast)){
					//context.ScaleCTM (0.5f, -1);
					context.TranslateCTM (0, 0);
					context.DrawImage (new RectangleF (0, 0, 57, 57), template.CGImage);
					context.SetRGBFillColor (1, 1, 1, 1);
					
					context.SelectFont ("Helvetica", 10f, CGTextEncoding.MacRoman);
					
					// Pretty lame way of measuring strings, as documented:
					var start = context.TextPosition.X;                 
					context.SetTextDrawingMode (CGTextDrawingMode.Invisible);
					context.ShowText (smallText);
					var width = context.TextPosition.X - start;
					
					var ns = new NSString (smallText);
					UIFont ff = UIFont.FromName ("Helvetica", 10);
					
					context.SetTextDrawingMode (CGTextDrawingMode.Fill);
					context.ShowTextAtPoint ((57-width)/2, 46, smallText);
					
					// The big string
					context.SelectFont ("Helvetica-Bold", 32, CGTextEncoding.MacRoman);                 
					start = context.TextPosition.X;
					context.SetTextDrawingMode (CGTextDrawingMode.Invisible);
					context.ShowText (bigText);
					width = context.TextPosition.X - start;
					
					context.SetRGBFillColor (0, 0, 0, 1);
					context.SetTextDrawingMode (CGTextDrawingMode.Fill);
					context.ShowTextAtPoint ((57-width)/2, 9, bigText);
					
					context.StrokePath ();
					
					return UIImage.FromImage (context.ToImage ());
				}
			}
		}

		public UIImage GetImage ()
		{
			UIImage returnImg = null;
			
			UIGraphics.BeginImageContext (this.Bounds.Size);
			
			using (CGContext context = UIGraphics.GetCurrentContext()) {
				context.SetStrokeColor (DrawingColor);
				context.SetLineWidth (5f);
				context.SetLineJoin (CGLineJoin.Round);
				context.SetLineCap (CGLineCap.Round);
				context.AddPath (this.drawPath);
				context.DrawPath (CGPathDrawingMode.Stroke);
				returnImg = UIGraphics.GetImageFromCurrentImageContext ();
			}
			
			UIGraphics.EndImageContext ();
			
			
			return returnImg;
		}

		public override void Draw (RectangleF rect)
		{
			base.Draw (rect);
			
			if (this.fingerDraw) {
				using (CGContext context = UIGraphics.GetCurrentContext()) {
					context.SetStrokeColor (DrawingColor);
					context.SetLineWidth (5f);
					context.SetLineJoin (CGLineJoin.Round);
					context.SetLineCap (CGLineCap.Round);
					this.drawPath.MoveToPoint (this.prevTouchLocation);
					this.drawPath.AddLineToPoint (this.touchLocation);
					context.AddPath (this.drawPath);
					context.DrawPath (CGPathDrawingMode.Stroke);

					Plot.Add (new DrawPoint (){
						Color = DrawingColor,
						Width = 5f,
						From = this.prevTouchLocation,
						To = this.touchLocation
					});

				}

			}    
		}
	}

	public class DrawPoint
	{
		public CGColor Color {
			get;
			set;
		}

		public float Width {
			get;
			set;
		}

		public PointF From {
			get;
			set;
		}

		public PointF To {
			get;
			set;
		}

	}

}

