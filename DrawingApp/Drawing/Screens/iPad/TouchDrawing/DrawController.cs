
using System;
using System.Drawing;

using MonoTouch.Foundation;
using MonoTouch.UIKit;
using System.IO;
using Example_Drawing.Screens.iPad.TouchDrawing;

namespace Example_Drawing
{
	public partial class DrawController : UIViewController
	{
		DrawView DrawView;

		public DrawController () : base ("DrawController", null)
		{
		}


		public override void DidReceiveMemoryWarning ()
		{
			// Releases the view if it doesn't have a superview.
			base.DidReceiveMemoryWarning ();
			
			// Release any cached data, images, etc that aren't in use.
		}

		public override void ViewWillAppear (bool animated)
		{
			base.ViewWillAppear (animated);

			
			DrawView = new DrawView (DrawingView.Frame, this);
			DrawingView.Add (DrawView);

			var image = UIImage.FromFile ("Images/PosturalDiagram.gif");
			//DrawingImage.Image = DrawView.MakeCalendarBadge(image, "Eric", "Testing");
			
			if (DrawOnImage.Image == null)
				DrawOnImage.Image = image;

		}

		public override void ViewDidLoad ()
		{
			base.ViewDidLoad ();
			
			// Perform any additional setup after loading the view, typically from a nib.


			EraseButton.TouchUpInside += delegate {
				DrawView.Clear();
			};

			SaveButton.TouchUpInside += delegate {
//				var image = DrawView.GetDrawingImage ();
				//DrawingImage.Image = DrawView.GetDrawingImage ();
				var image = UIImage.FromFile ("Images/PosturalDiagram.gif");
				//DrawingImage.Image = DrawView.MakeCalendarBadge(image, "Eric", "Testing");

				if (image != null)
					BackgroundView.BackgroundColor = UIColor.FromPatternImage(image);
				DrawingImage.Image = DrawView.GetDrawingImage();
			};

			DoRed.TouchUpInside += delegate {
				//				var image = DrawView.GetDrawingImage ();
				DrawView.DrawingColor = UIColor.Red.CGColor;
			};

			DoBlack.TouchUpInside += delegate {
				//				var image = DrawView.GetDrawingImage ();
				DrawView.DrawingColor = UIColor.Black.CGColor;
			};
		}
	}
}

