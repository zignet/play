// WARNING
//
// This file has been generated automatically by Xamarin Studio to store outlets and
// actions made in the Xcode designer. If it is removed, they will be lost.
// Manual changes to this file may not be handled correctly.
//
using MonoTouch.Foundation;

namespace Example_Drawing
{
	[Register ("DrawController")]
	partial class DrawController
	{
		[Outlet]
		MonoTouch.UIKit.UIImageView DrawOnImage { get; set; }

		[Outlet]
		MonoTouch.UIKit.UIView BackgroundView { get; set; }

		[Outlet]
		MonoTouch.UIKit.UIButton EraseButton { get; set; }

		[Outlet]
		MonoTouch.UIKit.UIButton SaveButton { get; set; }

		[Outlet]
		MonoTouch.UIKit.UIView DrawingView { get; set; }

		[Outlet]
		MonoTouch.UIKit.UIImageView DrawingImage { get; set; }

		[Outlet]
		MonoTouch.UIKit.UIButton DoBlack { get; set; }

		[Outlet]
		MonoTouch.UIKit.UIButton DoRed { get; set; }
		
		void ReleaseDesignerOutlets ()
		{
			if (DrawOnImage != null) {
				DrawOnImage.Dispose ();
				DrawOnImage = null;
			}

			if (BackgroundView != null) {
				BackgroundView.Dispose ();
				BackgroundView = null;
			}

			if (EraseButton != null) {
				EraseButton.Dispose ();
				EraseButton = null;
			}

			if (SaveButton != null) {
				SaveButton.Dispose ();
				SaveButton = null;
			}

			if (DrawingView != null) {
				DrawingView.Dispose ();
				DrawingView = null;
			}

			if (DrawingImage != null) {
				DrawingImage.Dispose ();
				DrawingImage = null;
			}

			if (DoBlack != null) {
				DoBlack.Dispose ();
				DoBlack = null;
			}

			if (DoRed != null) {
				DoRed.Dispose ();
				DoRed = null;
			}
		}
	}
}
