// WARNING
//
// This file has been generated automatically by Xamarin Studio to store outlets and
// actions made in the Xcode designer. If it is removed, they will be lost.
// Manual changes to this file may not be handled correctly.
//
using MonoTouch.Foundation;

namespace Example_Drawing.Screens.iPad.Home
{
	[Register ("HomeScreen")]
	partial class HomeScreen
	{
		[Outlet]
		MonoTouch.UIKit.UIButton btnDrawRectVsPath { get; set; }

		[Outlet]
		MonoTouch.UIKit.UIButton btnDrawUsingCGBitmapContext { get; set; }

		[Outlet]
		MonoTouch.UIKit.UIButton btnDrawUsingLayers { get; set; }

		[Outlet]
		MonoTouch.UIKit.UIButton EricTest { get; set; }

		[Outlet]
		MonoTouch.UIKit.UIButton btnOffScreenCoords { get; set; }

		[Outlet]
		MonoTouch.UIKit.UIButton btnOnScreenCoords { get; set; }

		[Outlet]
		MonoTouch.UIKit.UIButton btnOnScreenUncorrectedText { get; set; }

		[Outlet]
		MonoTouch.UIKit.UIButton btnOffScreenFlag { get; set; }

		[Outlet]
		MonoTouch.UIKit.UIButton btnImage { get; set; }

		[Outlet]
		MonoTouch.UIKit.UIButton btnPatterns { get; set; }

		[Outlet]
		MonoTouch.UIKit.UIButton btnStencilPattern { get; set; }

		[Outlet]
		MonoTouch.UIKit.UIButton btnShadows { get; set; }

		[Outlet]
		MonoTouch.UIKit.UIButton btnHitTesting { get; set; }

		[Outlet]
		MonoTouch.UIKit.UIButton btnTouchDrawing { get; set; }

		[Outlet]
		MonoTouch.UIKit.UIButton btnOnScreenFlag { get; set; }

		[Outlet]
		MonoTouch.UIKit.UIButton btnTransformations { get; set; }
		
		void ReleaseDesignerOutlets ()
		{
			if (btnDrawRectVsPath != null) {
				btnDrawRectVsPath.Dispose ();
				btnDrawRectVsPath = null;
			}

			if (btnDrawUsingCGBitmapContext != null) {
				btnDrawUsingCGBitmapContext.Dispose ();
				btnDrawUsingCGBitmapContext = null;
			}

			if (btnDrawUsingLayers != null) {
				btnDrawUsingLayers.Dispose ();
				btnDrawUsingLayers = null;
			}

			if (EricTest != null) {
				EricTest.Dispose ();
				EricTest = null;
			}

			if (btnOffScreenCoords != null) {
				btnOffScreenCoords.Dispose ();
				btnOffScreenCoords = null;
			}

			if (btnOnScreenCoords != null) {
				btnOnScreenCoords.Dispose ();
				btnOnScreenCoords = null;
			}

			if (btnOnScreenUncorrectedText != null) {
				btnOnScreenUncorrectedText.Dispose ();
				btnOnScreenUncorrectedText = null;
			}

			if (btnOffScreenFlag != null) {
				btnOffScreenFlag.Dispose ();
				btnOffScreenFlag = null;
			}

			if (btnImage != null) {
				btnImage.Dispose ();
				btnImage = null;
			}

			if (btnPatterns != null) {
				btnPatterns.Dispose ();
				btnPatterns = null;
			}

			if (btnStencilPattern != null) {
				btnStencilPattern.Dispose ();
				btnStencilPattern = null;
			}

			if (btnShadows != null) {
				btnShadows.Dispose ();
				btnShadows = null;
			}

			if (btnHitTesting != null) {
				btnHitTesting.Dispose ();
				btnHitTesting = null;
			}

			if (btnTouchDrawing != null) {
				btnTouchDrawing.Dispose ();
				btnTouchDrawing = null;
			}

			if (btnOnScreenFlag != null) {
				btnOnScreenFlag.Dispose ();
				btnOnScreenFlag = null;
			}

			if (btnTransformations != null) {
				btnTransformations.Dispose ();
				btnTransformations = null;
			}
		}
	}
}
